#FROM golang:alpine3.15
FROM golang:buster
RUN apt install ca-certificates curl gzip tar wget
RUN cd /tmp && curl https://raw.githubusercontent.com/kanisterio/kanister/master/scripts/get.sh | bash
RUN kanctl --version
